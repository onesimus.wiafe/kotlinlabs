package day1.lab2

// question 1
var funds : Double = 100.0
val pswd = "password"

fun main() {
    var input : String
    var cmd : List<String>

    while (true) {
        print("Command: ")
        input = readLine()!!
        cmd = input.split(" ")
        when (cmd[0].lowercase()) {
            // Each command goes here...
            "balance"-> balance() // question 2
            "deposit" -> when(cmd.size > 1) {
                true -> {when(cmd[1].toDoubleOrNull()) {
                        is Double -> deposit(cmd[1].toDouble())
                        else -> println("Enter a number.")
                    }
                }
                else -> println("Enter an amount to deposit")
            } // question 3 & 5
            "withdraw" -> when(cmd.size > 1) {
                true -> {when(cmd[1].toDoubleOrNull()) {
                        is Double -> withdraw(cmd[1].toDouble())
                        else -> println("Enter a number.")
                    }
                }
                else -> println("Enter an amount to deposit")
            } // question 4 & 5
            "exit" -> break
            else -> println("Invalid command")
        }
    }
}

// question 2
fun balance():Unit{
    println(funds)
}

// question 3
fun deposit(amountToDeposit: Double):Unit{
    if (amountToDeposit > 0) {
        funds += amountToDeposit
    }
}

// question 4
fun withdraw(amountToWithdraw: Double):Unit{
    print("Provide your password: ")
    val passwordInput = readLine()!!
    if (passwordInput == pswd){
        if (amountToWithdraw <= funds && amountToWithdraw > 0){
            funds -= amountToWithdraw
        }
    }
}