package day1.lab3.logic

import day1.lab3.util.Action
import day1.lab3.util.Actor
import day1.lab3.util.ForageAction
import day1.lab3.util.HuntAction
import java.math.RoundingMode
import java.text.DecimalFormat

class RandomAgent(override val name: String, private val prob : Double): Actor {
    override fun act(): Action {
        val df = DecimalFormat("#.#")
        df.roundingMode = RoundingMode.DOWN
        val randomValue: Double = df.format(Math.random()).toDouble()
        return if (randomValue == prob) ForageAction() else HuntAction("lion")
    }

    override fun perceive(vararg facts: Percept) {}

    override fun toString(): String {
        return name
    }
}