package day1.lab3.logic

import day1.lab3.util.Action
import day1.lab3.util.Actor
import day1.lab3.util.ForageAction
import day1.lab3.util.HuntAction

class CleverAgent(override val name: String) : Actor {
    private var sensed: Percept? = null
    override fun act(): Action {
        return if (sensed is Percept) HuntAction(sensed!!.value) else ForageAction()
    }

    override fun perceive(vararg facts: Percept) {
        if (facts.isNotEmpty()){
            for (fact in facts){
                sensed = Percept(fact.key, fact.value)
            }
        }else{
            sensed = null
        }
    }

    override fun toString(): String {
        return name
    }
}