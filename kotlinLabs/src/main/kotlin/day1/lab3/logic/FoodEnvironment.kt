package day1.lab3.logic

import day1.lab3.util.*
import kotlin.random.Random

class FoodEnvironment(vararg ags: Actor) : Environment(*ags) {
    val scores : MutableMap<Actor, Int> = mutableMapOf()
    init {
        for (actor in ags){
            scores[actor] = 0
        }
    }

   var animal : String? = null


    override fun step() {
        val randomValue : Double = Random.nextDouble()
        animal = if (randomValue > 0.5){
            "Dzata"
        }else {
            null
        }
        super.step()
    }

    override fun processAction(agent: Actor, act: Action) {
        if (act is ForageAction && scores.containsKey(agent)) {
            scores[agent] = scores[agent]!! + 1
            println("$agent is foraging. - ${scores[agent]}")
        }else if (act is HuntAction && animal is String){
            scores[agent] = scores[agent]!! + 2
            println("$agent sensed a $animal. - ${scores[agent]}")
        }
    }

    override fun sense(agent: Actor) = run {
        if (animal is String){
            agent.perceive(Percept("animal", animal!!))
        }else {
            agent.perceive()
        }
    }
}