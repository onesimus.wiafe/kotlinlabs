package day1.lab3.logic

import day1.lab3.util.Action
import day1.lab3.util.Actor
import day1.lab3.util.ForageAction

class SimpleAgent(override val name: String) : Actor {
    override fun act() : Action {
        return ForageAction()
    }
    override fun perceive(vararg facts : Percept) : Unit{}

    override fun toString(): String {
        return name
    }

}