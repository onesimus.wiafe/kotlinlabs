package day1.lab3.util

import day1.lab3.logic.Percept

interface Actor {
    val name : String
    fun act() : Action
    fun perceive(vararg facts : Percept) : Unit
}