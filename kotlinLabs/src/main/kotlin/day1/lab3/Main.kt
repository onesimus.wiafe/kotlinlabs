package day1.lab3

import day1.lab3.logic.CleverAgent
import day1.lab3.logic.FoodEnvironment
import day1.lab3.logic.RandomAgent
import day1.lab3.logic.SimpleAgent

fun main() {
    val sAgent = SimpleAgent("Kobby")
    println(sAgent)
    println("---------------------")

    val env1 = FoodEnvironment(SimpleAgent("Charlie"))
    env1.step()
    env1.step()
    println(env1.scores)
    println("----------------------")

    val env = FoodEnvironment(RandomAgent("Charlie",0.8), RandomAgent("Bob", 0.2), RandomAgent("Alice", 0.6))
    for (i in 1..100)
        env.step()
    println(env.scores)
    println("----------------------")

    val env2 = FoodEnvironment()
    for (i in 1..10) {
        env2.step()
        println(env2.animal)
    }
    println("----------------------")

    val env3 = FoodEnvironment(CleverAgent("Charlie"), RandomAgent("Bob", 0.2), SimpleAgent("Alice"))
    for (i in 1..100) {
        env3.step()
    }
    println(env3.scores)

}