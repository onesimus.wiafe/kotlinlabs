package day1

fun main(){
    // question 1
    println("Hello, World!")


    // question 2
    val hello: String = "Hello"
    println(hello)


    // question 3
    println("For y = 3x - 1: ")
    for (x in -5 .. 5){
        print("When x is $x, ")
        println("y is ${calculate(3, x, -1)}")
    }


    // question 4
    for (x in 0..20){
        if (calculate(3, x, 5) == calculate(4, x, 2)){
            println("The two lines: 3x + 5 and 4x + 2 intersect at the point x = $x and y = ${calculate(3, x, 5)}")
        }
    }

    println()
    intersection(3, 5, 4, 2)
    println()
    intersection(2, 1, 3, 3)
    println()
    intersection(-2, 3, 3, -2)
}

// question 3 and 4.
fun calculate(m : Int, x : Int, c: Int): Int{
    // y = mx + c - formula for a straight line
    return (m * x) + c
}

fun intersection(mOne: Int, cOne: Int, mTwo: Int, cTwo: Int): Unit{
    // This function takes the equations of two lines:
    // yOne = mOne * x + cOne and yTwo = mTwo * x + cTwo
    // and prints the X value they intersect at
    // or prints "Don't intersect" if they do not.
    // You only need to check x values between 0 and 100
    for (x in 0..100){
        if (calculate(mOne, x, cOne) == calculate(mTwo, x, cTwo)){
            println("The lines intersect at x = $x.")
        }else{
            println("Don't intersect")
        }
    }
}