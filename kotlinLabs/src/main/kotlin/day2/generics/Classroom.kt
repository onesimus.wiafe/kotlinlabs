package day2.generics


open class Classroom<T : Student>(vararg sts : T) : Iterable<T> {
    private val students : MutableList<T> = mutableListOf()
    init {
        for (s in sts) students.add(s)
    }

    override fun iterator(): Iterator<T> {
        return students.iterator()
    }
}