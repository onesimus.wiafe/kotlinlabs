package day2.generics

class Student(val name : String, private val grades : List<Double>) {
    fun averageGrade() : Double {
        return grades.reduce { a, b -> a + b } /grades.size
    }
}