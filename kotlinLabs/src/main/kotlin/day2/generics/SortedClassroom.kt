package day2.generics

class SortedClassroom(vararg sts: Student): Classroom<Student>(*sts) {

//    override fun iterator(){}

    inner class StudentSorter : Comparator<Student> {
        override fun compare(o1: Student?, o2: Student?): Int {
            if (o1 != null && o2 != null) {
                if ((o1.name == o2.name)) {
                    return if(o1.averageGrade() == o2.averageGrade()) {
                        0
                    }else if (o1.averageGrade() > o2.averageGrade()) {
                        1
                    } else {
                        1
                    }
                }
                else if ((o1.name > o2.name)) {
                    return if(o1.averageGrade() == o2.averageGrade()) {
                        1
                    } else if(o1.averageGrade() > o2.averageGrade()) {
                        1
                    } else {
                        -1
                    }
                }
                else {
                    return if(o1.averageGrade() == o2.averageGrade()) {
                        -1
                    } else if(o1.averageGrade() > o2.averageGrade()) {
                        -1
                    } else {
                        1
                    }
                }
            }
            return -1
        }
    }

//    override fun iterator(): Iterator<Student> {
//        return object : Iterator<Student>{
//            val state = from.sorted
//        }
//    }
}