package day2.tutor_group_oop

class CleverAgent(override val name: String): Actor {
    var sensed : Percept? = null

    override fun act(): Action {
        return when (sensed){
            is Percept -> HuntAction(sensed!!.value)
            else -> ForageAction()
        }
    }

    override fun perceive(vararg facts: Percept) {
        if (facts.isEmpty()){
            sensed = null
        }else {
            facts.forEach {
                sensed = it
            }
        }
    }

    override fun toString(): String {
        return name
    }
}