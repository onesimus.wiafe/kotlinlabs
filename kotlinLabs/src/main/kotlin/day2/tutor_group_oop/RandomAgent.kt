package day2.tutor_group_oop

import kotlin.random.Random

class RandomAgent(override val name: String, val prob : Double): Actor {

    override fun act(): Action {
        val randomVal = Random.nextDouble()
        return if (prob <= randomVal) {
            ForageAction()
        }else {
            NoAction()
        }
    }

    override fun perceive(vararg facts: Percept) {}
    override fun toString(): String {
        return name
    }
}