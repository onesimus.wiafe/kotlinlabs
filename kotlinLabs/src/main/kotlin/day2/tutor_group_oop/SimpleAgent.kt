package day2.tutor_group_oop

class SimpleAgent(override val name: String) : Actor {
    override fun act(): Action {
        return ForageAction()
    }

    override fun perceive(vararg facts: Percept) {
    }

    override fun toString(): String {
        return name
    }

}