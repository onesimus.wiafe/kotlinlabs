package day2.tutor_group_oop

interface Actor {
    val name : String
    fun act() : Action
    fun perceive(vararg facts : Percept) : Unit
}