package day2.tutor_group_oop

import kotlin.random.Random

class FoodEnvironment(vararg ags: Actor): Environment(*ags) {

    val scores : MutableMap<Actor, Int> = mutableMapOf()
    init {
        ags.forEach {
            scores[it] = 0
        }
    }

    var animal :String? = null

    override fun step() {
        val randomVal = Random.nextDouble()

        animal = if (randomVal >= 0.5){
            "Lion"
        }else {
            null
        }

        super.step()
    }

    override fun processAction(agent: Actor, act: Action) {
        when(act){
            is ForageAction -> {scores[agent] = scores[agent]!! + 1; println("$agent = ${scores[agent]}")}
            is NoAction -> {}
            is HuntAction -> { animal.let { scores[agent] = scores[agent]!! + 2 }; println("$agent = ${scores[agent]}")}
            else -> println("This Action does not exist.")
        }
    }

    override fun sense(agent: Actor) {
        when(animal) {
            is String -> agent.perceive(Percept("animal", animal!!))
            else -> agent.perceive()
        }

    }
}