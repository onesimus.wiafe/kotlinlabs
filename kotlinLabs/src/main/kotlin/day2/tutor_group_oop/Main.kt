package day2.tutor_group_oop

fun main(){
//    val me = SimpleAgent("Ony")
//    println(me.act())

//    val env = FoodEnvironment(SimpleAgent("Charlie"))
//    env.step()
//    println(env.scores)

//    val env = FoodEnvironment(RandomAgent("Charlie",0.8), RandomAgent("Bob", 0.2), RandomAgent("Alice", 0.6))
//    for (i in 1..100)
//        env.step()
//    println(env.scores)

//    val env = FoodEnvironment()
//    for (i in 1..10) {
//        env.step()
//        println(env.animal)
//    }

    val env = FoodEnvironment(CleverAgent("Charlie"), RandomAgent("Bob", 0.2), SimpleAgent("Alice"))
    for (i in 1..100) {
        env.step()
    }
    println(env.scores)
}